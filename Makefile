init: ## Build and update images
	docker-compose up --build -d
	$(MAKE) install-dependencies
	$(MAKE) run-db-migrations

up: ## Up all services
	docker-compose up -d
	$(MAKE) run-db-migrations

down: ## Down all services
	docker-compose down

terminal: ## Enter the php container shell
	docker exec -it project.api bash

install-dependencies: ### install composer dependencies
	docker exec project.api composer install

test: ## Run test suites
	docker exec project.api ./vendor/bin/phpunit

new-migration: ## Generate a new migration
	docker exec project.api ./vendor/bin/doctrine-migrations generate

run-db-migrations: wait-mysql-connection ## Run migrations
	docker exec project.api ./vendor/bin/doctrine-migrations migrate

wait-mysql-connection: ## Wait for MySql to be ready
	docker-compose run api ./bin/wait-mysql-connection
