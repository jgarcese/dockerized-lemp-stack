<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function PHPUnit\Framework\assertEquals;

class HealthzControllerTest extends WebTestCase
{
    private const HTTP_OK = 200;

    /** @test */
    public function shouldReturnStatusCode200ok(): void
    {
        $client = static::createClient();
        $client->request('GET', '/healthz');

        $responseStatusCode = $client->getResponse()->getStatusCode();

        $expectedResponseStatusCode = self::HTTP_OK;
        assertEquals($expectedResponseStatusCode, $responseStatusCode);
    }
}
