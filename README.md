# Dockerized Symfony PHP-FPM, MariaDB and NGINX for API development

## Introduction

A fully dockerized development environment for building an API, created following this post: https://jgarcese.gitlab.io/posts/php-mysql-ngnix-docker/


**PHP version:** 8.0.8

**NGINX version:** 1.20.1

**MariaDB Version:** latest

Includes PhpUnit, Xdebug, Doctrine DBAL and Doctrine Migrations.

First migration added a `users` table to the database, with email and password fields.

Also includes a health controller at /healthz


## Requirements

Docker and Docker Compose


## Instructions

Run `make init` the first time, then `make up`.

`make test` to run tests.

That's all, enjoy!


Jorge
