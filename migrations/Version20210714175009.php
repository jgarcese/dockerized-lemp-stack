<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210714175009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Added users table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            <<<SQL
            CREATE TABLE `users` (
            `id` char(36) NOT NULL DEFAULT '',
            `email` char(255) NOT NULL,
            `password` char(255) NOT NULL,
            PRIMARY KEY (`email`),
            CONSTRAINT uc_email UNIQUE (`email`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            SQL
        );
    }

    public function down(Schema $schema): void
    {
    }
}
