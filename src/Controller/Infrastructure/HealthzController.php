<?php

namespace App\Controller\Infrastructure;

use Symfony\Component\HttpFoundation\Response;

class HealthzController
{
    public function index(): Response
    {
        return new Response('ok', 200);
    }
}
